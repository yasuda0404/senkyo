//Main, Election
var container = d3.select("#container");
var ezone = electralUnit();
var chart = electralUnit();
//var _colors = d3.scale.category20().range();
var _colors = ["#ff9896","#c5b0d5","#1f77b4", "#2ca02c", "#98df8a", "#17becf", "#ff7f0e", "#bcbd22", "#d62728", "#e377c2", "#c49c94", "#9edae5",  "#8c564b",  "#9467bd", "#c7c7c7", "#7f7f7f", "#f7b6d2", "#dbdb8d", "#ffbb78", "#aec7e8"];
var _size = 20;
var _box;


function onRadioButton(){
    var dspls = document.getElementsByName("RBDisplay");
    var years = document.getElementsByName("RBYear");
    var val1 = "Display not selected";
    var val2 = "Year not selected";
    
    //get value of the selected radio button
    for(var i=0; i<dspls.length; i++){
        if (dspls[i].checked) {
            val1 = dspls[i].value;
            break;
        }
    }
    for(var i=0; i<years.length; i++){
        if (years[i].checked) {
            val2 = years[i].value;
            break;
        }
    }
    //console.log("radioButton checked. value= "+val1+","+val2)
    //
    if(val1 == "kuwari"){
        d3.select("#chart").style("display","none");
        d3.select("#scale").style("display","none");
    }else{
        d3.select("#chart").style("display","inherit");
        d3.select("#scale").style("display","inherit");
    }
    
    d3.select("#year").text(val2);  //年表示
    var _no = (val2=='2012') ? 0 : 1;   //_no=0:2012年 =1:2014年
    //console.log("onRadioButton _no= "+_no);
    ezone.disp(val1)
        .no(_no)
        .show();
    chart.disp(val1)
        .no(_no)
        .show();
}

function onCKName(){
    //console.log("onCKname");
    var ck = document.getElementById("CKname");
    if(ck.checked){
        ezone.nameon();
    }else{
        ezone.nameoff();
    }
}


function render(error, data0, data11, data21, data12, data22){
    //console.log("election.render()");
    //set GUI
    ezone.addSeries(data11)
        .addSeries(data21)
        .party(data0)
        .base(d3.select("#map"))
        .sel(0)
        .no(1)
        .size(20)
        .width(800)
        .height(800)
        .colorKuwari(d3.scale.category10().range())
        .colorKekka(_colors)
        .margins({top: 20, left: 0, right: 0, bottom: 0})
        .tooltip(d3.select("#tooltip1"))
        .tipoffset([10,-30])
        .count(d3.select("#count_map"))
        ;
    
    chart.addSeries(data12)
        .addSeries(data22)
        .party(data0)
        .base(d3.select("#chart"))
        .sel(1)
        .no(1)
        .size(20)
        .width(400)
        .height(300)
        .colorKuwari(d3.scale.category10().range())
        .colorKekka(_colors)
        .margins({top: 10, left: 0, right: 0, bottom: 0})
        .tooltip(d3.select("#tooltip2"))
        .tipoffset([10,0])
        .count(d3.select("#count_chart"))
        ;
    //
    ezone.render();
    chart.render();
    onRadioButton();
    
    drawScale(data0);
}

function drawScale(data){
    
    //color scale
    _box = d3.select("#scale")
        .append("svg")
        .append("g")
        .selectAll("rect")
        .data(data)
        .enter()
            .append("rect")
            .attr("x", function(d,i){
                return 10;
            })
            .attr("y", function(d,i){
                return i*_size;
            })
            .attr("height",_size)
            .attr("width", _size)
            .attr("fill-opacity", 0.8)
            .attr("fill", function(d,i){
                return _colors[i];
            })
            .on("mouseover", function(d){
                ezone.filter(d.party);
                chart.filter(d.party);
            })
            .on("mouseout", function(d){
                ezone.unfilter(d.party);
                chart.unfilter(d.party);
        })
    
    _text = d3.select("#scale")
        .select("svg")
        .append("g")
        .selectAll("text")
        .data(data)
        .enter()
        .append("text")
        .attr("x", function(d,i){
            return "34px";
        })
        .attr("y", function(d,i){
            return (i+0.5)*_size;
        })
        .text(function(d){
            return d.party;
        })
};

console.log(_colors);
queue()
    .defer(d3.csv, "dataset/party.csv")
    .defer(d3.csv, "dataset/shosenkyoku2012.csv")
    .defer(d3.csv, "dataset/shosenkyoku2014.csv")
    .defer(d3.csv, "dataset/hirei2012.csv")
    .defer(d3.csv, "dataset/hirei2014.csv")
    .await(render);