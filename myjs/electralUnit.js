function electralUnit(){
    var     _chart = {},
            _x, _y,
            _data = [],
            _svg, 
            _map,
            _tip,
            _count
            ;
    
    var _base,
        _width  = 900,
        _height = 900,
        _size   = 16,
        _margins= {top: 0, left: 0, right: 0, bottom: 0},
        _colors = d3.scale.category20().range(),
        _colorKuwari =  d3.scale.category20().range(), 
        _colorKekka =  d3.scale.category20().range(), 
        _no     = 0,
        _column = 20,
        _sel    = 0,
        _disp = "kuwari",
        _year = 2012,
        _tipoffset = [0,0],
        _opacityOn  = 0.9,
        _opacityOff = 0.05,
        _countno = 0;
    
    var _party;
    //var _party = ["自民","公明","無所属","民主","維新","みんな","未来","共産","社民","大地","国民","改革","幸福","生活","改革","次世代","支持なし"];
    
    //--------------------------------------------------------------
    //private functions
    _chart.render = function(){
        //
        console.log("_chart.render");
        if (!_svg) {
            _svg = _base.append("svg")
                    .attr("height", _height+"px")
                    .attr("width", _width+"px")
                    .attr("class", "chart");
        }
        
        renderBody();
    };
    
    function renderBody(){
        var data = _data[_no];
        //console.log("_data.length "+data.length+" "+_no);
        if(!_map){
            console.log("create _map");
            _map = _svg.append("g")
            .attr("transform", function(){
                return "translate(" + x0() + "," + y1() + ")";});
        }
        //console.log("_map bedore selectAll")
        _map
        .selectAll("rect")
        .data(data)
        .enter()
            .append("rect")
            .attr("x", function(d,i){
                if(_sel == 0) {
                    return _size*d.x;
                }else{
                    var xp = i % _column;
                    return _size*xp;
                }
            })
            .attr("y", function(d,i){
                if(_sel == 0) {
                    return _size*d.y;
                }else{
                    var yp = Math.floor(i/_column);
                    return _size*yp;
                }
            })
            .attr("height",_size)
            .attr("width", _size)
            .attr("fill-opacity", _opacityOn)
            .attr("fill", function(d){
                if(_disp == "kuwari"){
                    //選挙区割
                    if(_sel == 0) {
                        //小選挙区
                        var n = d.id % _colorKuwari.length;
                        return _colorKuwari[n];
                    }else{
                        //比例区
                        return "#eee";
                    };
                }else{
                    //政党別
                    for(var j=0;j<_party.length;j++){
                        var obj = _party[j];
                        //console.log(obj+","+obj.party)
                        if(d.party == obj.party){
                            return _colorKekka[j];
                            break;
                        }
                    }
                }
            })
            .on("mouseover", function(d){
                _tip.style("display","inherit");
                var xp = parseFloat(d3.select(this).attr("x")) + _tipoffset[0];
                var yp = parseFloat(d3.select(this).attr("y")) + _tipoffset[1];
                //console.log(xp+" , "+yp);
                _tip.style("top", yp + "px")
                    .style("left", xp + "px");
                
                var str = "";
                if(_disp != "kuwari"){
                    str = d.party;
                }
                if(_sel == 0){
                    _tip.select("p#line1")
                        .text(d.pref +" " + d.no + "区");
                    _tip.select("p#line2")
                        .text(str);
                }else{
                    _tip.select("#line2")
                        .text(str);
                }
            })
            .on("mouseout", function(d){
                _tip.style("display","none");
                _tip.selectAll("p")
                    .text("");
            })
        ;
            
            //都道府県名表示
            _map.selectAll("rect")
                .each(function(d){
                    if((d.xc>0)&&(d.yc>0)){
                        //console.log(d.xc+","+d.yc);
                        var xc = _size*d.xc;
                        var yc = _size*d.yc - _size/2;
                        d3.select(this.parentNode)
                            .append("text")
                            .attr("x",xc)
                            .attr("y",yc)
                            .text(d.pref);
                    }
            })
            
            //Draw border lines
           _map.selectAll("rect")
            .attr("class",function(d){
                    return checkAdjacent([d.x,d.y],d.pref);
           })
        
    }
    
    function checkAdjacent(myLoc,myName){
        var data = _data[_no];
        //console.log("checkAdjacent "+myName)
        for(var i=0;i < data.length;i++){
            var dx = data[i]["x"] - myLoc[0]; //x位置の差
            var dy = data[i]["y"] - myLoc[1]; //y位置の差
            var str = "dummy";
            if((Math.abs(dx)==1)&&(Math.abs(dy)==1)){
                if( myName != data[i]["pref"]){
                    if(dx == 1){
                        str = str + "boarder-right ";
                    }
                    if(dx ==-1){str = str + "boarder-left ";}
                    if(dy == 1){str = str + "boarder-bottom ";}
                    if(dy ==-1){str = str + "boarder-top ";}
                }
            }
            return str;
        }
    }
    
    function x0() {
        return _margins.left;
    }
    function y0() {
        return _margins.top;
    }
    function x1() {
        return _width - _margins.right;
    }
    function y1() {
        return _margins.top;
    }
    function quadrantWidth() {
        //console.log("quadrantWidth "+_width+","+_margins.left+","+_margins.right);
        return _width - _margins.left - _margins.right;
    }
    function quadrantHeight() {
        return _height - _margins.top - _margins.bottom;
    }
    
    //public functions
    _chart.base = function (base) {
        if (!arguments.length) return _base;
        _base = base;
        return _chart;
    };
    _chart.width = function (w) {
        if (!arguments.length) return _width;
        _width = w;
        return _chart;
    };
    _chart.height = function (h) {
        if (!arguments.length) return _height;
        _height = h;
        return _chart;
    };
    _chart.size = function (s) {
        if (!arguments.length) return _size;
        _size = s;
        return _chart;
    };
    _chart.margins = function (m) {
        if (!arguments.length) return _margins;
        _margins = m;
        return _chart;
    };
    _chart.x = function (x) {
        if (!arguments.length) return _x;
        _x = x;
        return _chart;
    };
    _chart.y = function (y) {
        if (!arguments.length) return _y;
        _y = y;
        return _chart;
    };
    _chart.addSeries = function (series) {
        _data.push(series);
        return _chart;
    };
    _chart.colorKuwari = function (c) {
        if (!arguments.length) return _colorKuwari;
        _colorKuwari = c;
        return _chart;
    };
    _chart.colorKekka = function (c) {
        if (!arguments.length) return _colorKekka;
        _colorKekka = c;
        return _chart;
    };
    
    _chart.no = function (no) {
        if (!arguments.length) return _no;
        _no = no;
        return _chart;
    };
    _chart.sel = function (sel) {
        if (!arguments.length) return _sel;
        _sel = sel;
        return _chart;
    };
    
    _chart.party = function (party) {
        if (!arguments.length) return _party;
        _party = party;
        return _chart;
    };
    _chart.disp = function (disp) {
        if (!arguments.length) return _disp;
        _disp = disp;
        return _chart;
    };
    _chart.year = function (year) {
        if (!arguments.length) return _year;
        _year = year;
        return _chart;
    };
    
    _chart.nameon = function () {
        _map.selectAll("text")
            .style("display","inherit");
    }
    _chart.nameoff = function () {
        _map.selectAll("text")
            .style("display","none");
    };
    _chart.opacityOn = function (opOn) {
        if (!arguments.length) return _opacityOn;
        _opacityOn = opOn;
        return _chart;
    }
    _chart.opacityOff = function (opOff) {
        if (!arguments.length) return _opacityOff;
        _opacityOff = opOff;
        return _chart;
    }
    _chart.tooltip = function (tip) {
        if (!arguments.length) return _tip;
        _tip = tip;
        return _chart;
    };
    _chart.tipoffset = function (tipoffset) {
        if (!arguments.length) return _tipoffset;
        _tipoffset = tipoffset;
        return _chart;
    };
    _chart.count = function (count) {
        if (!arguments.length) return _count;
        _count = count;
        return _chart;
    };
    
    _chart.filter = function(pty) {
        _countno = 0;
        var partyno;
        for(var j=0;j<_party.length;j++){
            var obj = _party[j];
            //console.log(obj+","+obj.party)
            if(pty == obj.party){
                partyno = j;
                break;
            }
        }
        
        _map.selectAll("rect")
            .style("opacity", function(d){
                if(d.party == pty){
                    _countno++;
                    return _opacityOn;
                }else{
                    return _opacityOff;
                }
            })
        
        _count
            .style("display","inherit")
            .text(_countno)
            .style("color", function(){
                if(_disp == "kuwari"){
                    return "#eee"
                }else{
                    //政党別
                    return _colorKekka[partyno];
                }
            });
        //console.log("_countno= "+_countno)
    };
    _chart.unfilter = function(){
        _countno = 0;
        _map.selectAll("rect")
            .style("opacity", _opacityOn);
        _count
            .style("display","none")
            .text(_countno);
    };
    
    _chart.show = function(){
        console.log("_chart.show()");
        //remove svg elements
        _map.selectAll("rect")
                .remove();
            _map.selectAll("text")
                .remove();
        //
        renderBody();
    }
            

    //--------------------------------------------------------------

    return _chart;
}

